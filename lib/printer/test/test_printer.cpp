#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>

#include <Ice/Ice.h>
#include <printer.h>
#include <printeri/printeri.h>

#include <gtest/gtest.h>


struct PrinterIFixturs : public ::testing::Test {
    Ice::Current context;
    std::string text;

    PrinterIFixturs() {
        text = "Hello world";
    }

    ~PrinterIFixturs() {
        text = "";
    }
};



TEST_F(PrinterIFixturs , Printer_Pass) {
    auto servant = std::make_shared<PrinterI>();

    ::testing::internal::CaptureStdout();

    servant->printString(text, context);

    std::string capturedStdout = ::testing::internal::GetCapturedStdout();

    EXPECT_EQ(text + "\n", capturedStdout);
}
