
//
// c++ -I. -DICE_CPP11_MAPPING -c Printer.cpp Server.cpp
//
// c++ -o server Printer.o Server.o -lIce++11
//


#include <Ice/Ice.h>
#include <printer.h>
#include <printeri/printeri.h>


int main(int argc, char* argv[]) {
    try {
        Ice::CommunicatorHolder ich(argc, argv);
        auto adapter = ich->createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -p 10000");

        auto servant = std::make_shared<PrinterI>();
        adapter->add(servant, Ice::stringToIdentity("SimplePrinter"));
        adapter->activate();

        ich->waitForShutdown();
    } catch (const std::exception& e) {
        std::cerr << e.what() << "\n";
        return 1;
    }

    return 0;
}
