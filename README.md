# Cmake Demo Project with ZeroC

---
This is a demo cmake project using ZeroC and C/C++

---

## Key features:

 - Cmake is used to as build-system generator
 - Uses ccashe for debug builds (the can speed up build times significantly)
 - Build release builds
 - Gtest is the default unit-test framework
 - Debian install package
 - Switch to build with address sanitizer


## Directory structure

```
.
├── app
│   ├── client
│   │   └── client.cpp
│   ├── CMakeLists.txt
│   └── server
│       └── server.cpp
├── CMakeLists.txt
├── lib
│   ├── CMakeLists.txt
│   └── printer
│       └── printer.ice
├── LICENSE
├── postinst
├── ProjectVersion.h.in      <---- Configuration file to generate version header files
├── README.md
├── scripts
│   ├── build.sh            <---- shell script to invoke cmake build generator
│   ├── install_benchmark.sh
│   └── install_gtest.sh
└── toolchain_clang.cmake    <--- toolchain file for clang

```

## To Build Project


 - ./scripts/build.sh -dce
 - build options ( ./scripts/build.sh -u)

```
build.sh  v1.00.02.00

usage:
    -G    Run all unittest
    -p    Build install package
    -r    Release build

    -h    Show this menu


Advanced:
    -d    Delete build directory
    -c    Generate build system
    -e    Build project
    -l    Build with shared libraries
    -b    Build benchmarks
    -g    Run all unit-tests
    -C    Build with Clang compiler
    -s    Build with sanitizer flags on

```

